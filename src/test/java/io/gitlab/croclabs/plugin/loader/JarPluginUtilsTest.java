package io.gitlab.croclabs.plugin.loader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.stream.Collectors;

class JarPluginUtilsTest {

	@Test
	void load() {
		JarPlugins plugins = JarPluginUtils.load(".\\plugins");
		Set<String> s = plugins.all().stream()
				.map(JarPlugin::get)
				.map(Object::toString)
				.collect(Collectors.toSet());

		Assertions.assertFalse(s.isEmpty());
	}
}