package io.gitlab.croclabs.plugin.loader;

public class JarPluginLoaderException extends RuntimeException {
	public JarPluginLoaderException() {
	}

	public JarPluginLoaderException(String message) {
		super(message);
	}

	public JarPluginLoaderException(String message, Throwable cause) {
		super(message, cause);
	}

	public JarPluginLoaderException(Throwable cause) {
		super(cause);
	}

	public JarPluginLoaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
