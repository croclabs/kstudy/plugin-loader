package io.gitlab.croclabs.plugin.loader;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarPlugin<T> {
	final Class<T> clazz;
	final Class<?>[] interfaces;
	final Map<String, Object> config;

	@SuppressWarnings("unchecked")
	JarPlugin(File file, JarClassLoader jarClassLoader) {
		try (JarFile jarFile = new JarFile(file)) {
			JarEntry jarEntry = jarFile.getJarEntry("plugin.json");

			if (jarEntry == null) {
				throw new JarPluginLoaderException("Could not create plugin, no plugin.json found");
			}

			String configStr = new String(jarFile.getInputStream(jarEntry).readAllBytes(), StandardCharsets.UTF_8);
			config = new ObjectMapper().readValue(configStr, new TypeReference<>() { });

			String main = (String) config.get("main");

			if (main == null || main.isBlank()) {
				throw new JarPluginLoaderException("Could not create plugin, no main class in config");
			}

			clazz = (Class<T>) Class.forName(main, true, jarClassLoader);
			interfaces = clazz.getInterfaces();
		} catch (IOException | ClassNotFoundException e) {
			throw new JarPluginLoaderException("Could not create plugin", e);
		}
	}

	public T get() {
		try {
			return clazz.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			throw new JarPluginLoaderException("Could not create plugin instance", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <R> R get(Class<R> clazz) {
		return (R) get();
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public Class<?>[] getInterfaces() {
		return interfaces;
	}

	public Map<String, Object> getConfig() {
		return new HashMap<>(config);
	}
}
