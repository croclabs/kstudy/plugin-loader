package io.gitlab.croclabs.plugin.loader;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class JarPlugins {
	Set<JarPlugin<?>> plugins = new HashSet<>();

	public Set<JarPlugin<?>> all() {
		return new HashSet<>(plugins);
	}

	@SuppressWarnings("unchecked")
	public <T> Set<JarPlugin<T>> find(Class<T> interfaceClass) {
		return plugins.stream()
				.filter(p -> Arrays.asList(p.interfaces).contains(interfaceClass))
				.map(p -> (JarPlugin<T>) p)
				.collect(Collectors.toSet());
	}

	public <T> JarPlugin<T> findFirst(Class<T> interfaceClass) {
		return find(interfaceClass).stream()
				.findFirst()
				.orElse(null);
	}
}
