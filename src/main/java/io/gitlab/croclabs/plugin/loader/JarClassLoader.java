package io.gitlab.croclabs.plugin.loader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class JarClassLoader extends URLClassLoader {
	JarPlugin<?> plugin;

	public JarClassLoader(File file, ClassLoader parent) throws MalformedURLException {
		super(new URL[]{file.toURI().toURL()}, parent);
		plugin = new JarPlugin<>(file, this);
	}
}
