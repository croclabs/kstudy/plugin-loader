package io.gitlab.croclabs.plugin.loader;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Arrays;

public final class JarPluginUtils {
	private JarPluginUtils() {
		super();
	}

	public static JarPlugins load() {
		String dir = System.getenv("PLUGIN_DIR");

		if (dir != null) {
			return load(dir);
		}

		dir = System.getProperty("plugin.dir");

		if (dir != null) {
			return load(dir);
		}

		dir = System.getProperty("catalina.base");

		if (dir != null) {
			return load(dir + "/plugins");
		}

		return new JarPlugins();
	}

	public static JarPlugins load(String directory) {
		File dir = new File(directory);
		JarPlugins plugins = new JarPlugins();

		if (dir.isDirectory()) {
			File[] files = dir.listFiles();

			if (files == null) {
				return plugins;
			}

			Arrays.stream(files)
					.filter(f -> f.getName().endsWith(".jar"))
					.map(JarPluginUtils::getJarClassLoader)
					.map(jcl -> jcl.plugin)
					.forEach(p -> plugins.plugins.add(p));
		}

		return plugins;
	}

	private static JarClassLoader getJarClassLoader(File f) {
		try {
			return new JarClassLoader(f, JarPluginUtils.class.getClassLoader());
		} catch (MalformedURLException e) {
			throw new JarPluginLoaderException("Could not load plugins", e);
		}
	}
}
