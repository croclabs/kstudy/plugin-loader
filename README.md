# Usage

Place the JAR plugins inside your destined plugins folder. In your
project, call `JarPluginUtils.load([directory url])` (url is not needed,
see next chapter). This will get a class `JarPlugins` holding all found
plugins inside a `Set`. You can then filter via Interfaces the plugin
implements or get all plugins that were found. Once you got your wanted
`JarPlugin`, you can call `get`on it to receive an instance of the defined
class in the `plugin.json` file.

# Setting the directory

There are 4 ways to set the plugins directory:

- Environment variable: `PLUGIN_DIR`
- System property: `-Dplugin.dir`
- `plugins` folder in Catalina base (web projects on tomcat) 
- Setting the directory manually in `load` method

# `plugin.json`

Each plugin needs a `plugin.json` at the root of the `resources` folder.
In this JSON there needs to be a field named `main` holding the
_fully qualified class name_.

```json
{
  "main" : "[package path to class].[class name]"
}
```

Anything else added here is just extra information that you can provide
about your plugin.

The specified class MUST have a default constructor!
